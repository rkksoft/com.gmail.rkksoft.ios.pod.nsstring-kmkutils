//
//  NSString+KMKUtils.h
//  NSString+KMKUtils
//
//  Copyright (c) 2015, Yauheni Klishevich.
//  Released under the MIT license.
//

#import <UIKit/UIKit.h>


@interface NSString (KMKUtils)


#pragma mark - Factory methods -

/**
 The standard format for UUIDs represented in ASCII is a string punctuated by hyphens, for example 68753A44-4D6F-1226-9C60-0050E4C00067.
 */
+ (NSString *)stringWithGeneratedUUID;


#pragma mark - Drawing String in a Given Area

/**
 @param lineBreakMode For this argument see comment to `kmk_boundingRectWithSize:options:attributes:context:`
 Returns the size of the string if it were to be rendered with the specified font and line attributes on a single line.
 */
- (CGSize)kmk_oneLineSizeConstrainedToWidth:(CGFloat)width
                                       font:(UIFont *)font
                              lineBreakMode:(NSLineBreakMode)lineBreakMode;

/**
 Returns the size preferred for the current font and constrained to the specified size.
 @param lineBreakMode Only `NSLineBreakByWordWrapping` is honored. All other options are treated as in case of `NSLineBreakByCharWrapping`.
 For details reason why this is done see comment to `kmk_boundingRectWithSize:options:attributes:context:`.
 */
- (CGSize)kmk_multilineSizeConstrainedToSize:(CGSize)size
                                        font:(UIFont *)font
                               lineBreakMode:(NSLineBreakMode)lineBreakMode;

/**
 @param numberOfLines To remove any maximum limit, and use as many lines as needed, set the value of this property to 0.
 @param lineBreakMode If `numberOfLines` != 1 then only `NSLineBreakByWordWrapping` is honored. All other options are treated as in case of `NSLineBreakByCharWrapping`.
 For details why this is done so see comment to `kmk_boundingRectWithSize:options:attributes:context:`.
 
 Method may be especially useful when it is needed to calculate the height of the label without creating it. For example
 such situation may occur when calculating the height of the cell without creating the cell itself.
 
 @return The size for label with the specified parameters. Returned value is always intergral (values are raised to the nearest hiegher integer value).
 */
- (CGSize)kmk_multilineSizeConstrainedToWidth:(CGFloat)width
                                numberOfLines:(NSInteger)numberOfLines
                                         font:(UIFont *)font
                                lineBreakMode:(NSLineBreakMode)lineBreakMode;

//TODO: the reason of improper behaviour when using described here approach is that returned attributes have lineBreakMode `NSLineBreakByTruncatingTail`, correction is needed
/**
 Returned width is the same is in `size` and height is not greater than `size.height`.
 
 This method does the same as `-[NSString boundingRectWithSize:options:attributes:context:]` method but we add here some valuable comments about this method.
 
 Do not use approach like [titleLabel.attributedText attributesAtIndex:0 effectiveRange:NULL] to get attributes and then pass them into this method or into
 `-[NSString boundingRectWithSize:options:attributes:context:]` method directly becuase this may lead to incorrect results.
 
 For example for label with the following result of "po [titleLabel.attributedText attributesAtIndex:0 effectiveRange:NULL]":
 
 {
 NSColor = "UIDeviceWhiteColorSpace 0 1";
 NSFont = "<UICTFont: 0x7f9951e82a90> font-family: \"HelveticaNeue-Medium\"; font-weight: normal; font-style: normal; font-size: 17.00pt";
 NSParagraphStyle = "Alignment 0, LineSpacing 0, ParagraphSpacing 0, ParagraphSpacingBefore 0, HeadIndent 0, TailIndent 0, FirstLineHeadIndent 0, LineHeight 0/0, LineHeightMultiple 0, LineBreakMode 4, Tabs (\n    28L,\n    56L,\n    84L,\n    112L,\n    140L,\n    168L,\n    196L,\n    224L,\n    252L,\n    280L,\n    308L,\n    336L\n), DefaultTabInterval 0, Blocks (null), Lists (null), BaseWritingDirection -1, HyphenationFactor 0, TighteningFactor 0, HeaderLevel 0";
 NSShadow = "NSShadow {0, -1} color = {(null)}";
 }
 
 `-[NSString boundingRectWithSize:options:attributes:context:]` method returned 20.757000000000005 instead of 41.021000000000008
 
 Instead use approach like the one used in implementation of `-[NSString kmk_multilineSizeConstrainedToWidth:numberOfLines:font:lineBreakMode:]` method
 
 @param size If height is 0, then result is (size.width, 0) although `-[NSString boundingRectWithSize:options:attributes:context:]` in this case perceives height as infinite.
 @param attributes If lineBreakMode in NSParagraphStyle is:
 1. `NSLineBreakByTruncatingTail` - result always corresponds as if it was one-line label. Width is not more then size.width.
 2. `NSLineBreakByClipping` - result always corresponds as if it was one-line label. Constraint to width is honered although `-[NSString boundingRectWithSize:options:attributes:context:]` method in this case ignores constraint to width.
 3. `NSLineBreakByTruncatingHead` - result always corresponds as if it was one-line label. Width is not more then size.width.
 4. `NSLineBreakByTruncatingMiddle` - result always corresponds as if it was one-line label. Width is not more then size.width.
 */
- (CGRect)kmk_boundingRectWithSize:(CGSize)size
                           options:(NSStringDrawingOptions)options
                        attributes:(NSDictionary *)attributes
                           context:(NSStringDrawingContext *)context NS_AVAILABLE_IOS(7_0);

@end
