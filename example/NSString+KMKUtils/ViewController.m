//
//  ViewController.m
//  NSString+KMKUtils
//
//  Created by Eugene Klishevich on 30/10/15.
//  Copyright © 2015 Yauheni Klishevich. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
